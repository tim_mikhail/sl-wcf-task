﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MessagesRegister.ServiceReference1;

namespace MessagesRegister
{
	public partial class MainPage : UserControl
	{
		public MainPage()
		{
			InitializeComponent();
		}

		private void buttonSend_Click(object sender, RoutedEventArgs e)
		{
			MessagesServiceClient client = new MessagesServiceClient();

			// Use the 'client' variable to call operations on the service.
			client.RegisterAsync(new Message{Text = textBoxMsg.Text});
			// Always close the client.
			//client.Close();
			
			labelMsgs.Content = "send clicked";
		}

		private void buttonReload_Click(object sender, RoutedEventArgs e)
		{
			labelMsgs.Content = "reload clicked";
			MessagesServiceClient client = new MessagesServiceClient();
			client.GetAsync();
			client.GetCompleted += (o, args) =>
			{
				var builder = new StringBuilder();
				foreach (var msg in args.Result)
				{
					builder.Append(msg);
				}

				labelMsgs.Content = builder.ToString();
			};
		}
	}
}
