﻿using System;
using System.Runtime.Serialization;

namespace MessagesRegister.Web
{
	
	/// <summary>
	/// Сообщение
	/// </summary>
	[DataContract]
	public class Message
	{
		/// <summary>
		/// Идентификатор
		/// </summary>
		[DataMember]
		public Guid Id { get; set; }

		/// <summary>
		/// Текст
		/// </summary>
		[DataMember]
		public String Text { get; set; }
		
		/// <summary>
		/// Дата время регистрации
		/// </summary>
		[DataMember]
		public DateTime RegDate { get; set; }
	}
}