﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MessagesRegister.Web
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMessagesService" in both code and config file together.
	[ServiceContract]
	public interface IMessagesService
	{
		[OperationContract]
		void Register(Message msg);

		[OperationContract]
		IEnumerable<Message> Get();
	}
}
