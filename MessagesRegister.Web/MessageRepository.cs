﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagesRegister.Web
{


	public class MessageRepository
	{
		private ConcurrentBag<Message> _messages = new ConcurrentBag<Message>();

		private static volatile MessageRepository instance;
		private static object syncRoot = new Object();

		private MessageRepository()
		{
		}

		public static MessageRepository Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new MessageRepository();
					}
				}

				return instance;
			}
		}

		public void Add(Message msg)
		{
			_messages.Add(msg);
		}

		public IEnumerable<Message> Get()
		{
			return _messages.AsEnumerable();
		}
	}
}